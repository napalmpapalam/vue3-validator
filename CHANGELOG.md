# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]
## [0.1.0-rc.1] - 2021-09-02
### "Under the hood" changes
- Initial release

[Unreleased]: https://gitlab.com/napalmpapalam/vue3-validator/compare/0.1.0-rc.1...main
[0.1.0-rc.1]: https://gitlab.com/napalmpapalam/vue3-validator/tags/0.1.0-rc.1
