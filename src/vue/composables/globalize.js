import { useI18n } from 'vue-i18n'

export function globalize (translationsId, interpolationOps) {
  const { t } = useI18n({ useScope: 'global' })
  // eslint-disable-next-line vue-i18n/no-dynamic-keys
  return t(translationsId, interpolationOps)
}
