import packageJson from '../package.json'

export default {
  /**
   * App name
   */
  APP_NAME: 'vue3-validator',

  /**
   * Should be populated by DevOps team during the deployment
   * The field being displayed on login screen.
   */
  BUILD_VERSION: packageJson.version,
}
