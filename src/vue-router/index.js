import { createRouter, createWebHistory } from 'vue-router'
import vueRoutes from '@/vue-router/routes'

const routes = [
  {
    path: '/:catchAll(.*)',
    redirect: vueRoutes.app,
  },
  {
    path: '/',
    name: vueRoutes.app.name,
    redirect: vueRoutes.forms,
    component: () => import(/* webpackChunkName: "app-pages" */ '@/vue/AppContent'),
    children: [
      {
        path: '/forms',
        name: vueRoutes.forms.name,
        component: () => import(/* webpackChunkName: "app-pages" */ '@/vue/pages/Forms'),
      },
      {
        path: '/signup',
        name: vueRoutes.signup.name,
        component: () => import(/* webpackChunkName: "app-pages" */ '@/vue/pages/SignUp'),
      },
    ],
  },
]

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes,
  scrollBehavior: _ => ({ x: 0, y: 0 }),
})

export default router
export { vueRoutes }
export { useRouter, useRoute, onBeforeRouteLeave } from 'vue-router'
