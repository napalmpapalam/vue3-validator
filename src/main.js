import '@/scss/app.scss'

import router from '@/vue-router'
import config from '@/config'
import App from '@/vue/App.vue'
import vueRoutes from '@/vue-router/routes'

import { createApp, getCurrentInstance, h } from 'vue'
import { i18n } from '@/i18n'
import { globalize } from '@/vue/composables/globalize'

import { AppButton } from '@/vue/common'

const app = createApp({
  setup () {
    const app = getCurrentInstance()

    app.appContext.config.globalProperties.$t = globalize
  },
  render: () => h(App),
})

app.use(router).use(i18n)

app.config.globalProperties.$config = config
app.config.globalProperties.$routes = vueRoutes

app.config.errorHandler = error => { console.error(error) }

app.component('AppButton', AppButton)

app.mount('#app')
